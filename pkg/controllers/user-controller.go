package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/adeindra6/oninyon-test/pkg/models"
	"github.com/adeindra6/oninyon-test/pkg/utils"
	"github.com/gorilla/mux"
)

var NewUser models.User

type ErrMessage struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Code    int64  `json:"code"`
}

func CreateUser(w http.ResponseWriter, r *http.Request) {
	CreateUser := &models.User{}
	utils.ParseBody(r, CreateUser)
	u := CreateUser.CreateUser()
	res, err := json.Marshal(u)
	if err != nil {
		err_msg := ErrMessage{
			Status:  "ERROR",
			Message: "Error while creating new user",
			Code:    http.StatusInternalServerError,
		}
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err_msg)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	newUsers := models.GetAllUser()
	res, err := json.Marshal(newUsers)
	if err != nil {
		fmt.Print("Error when fetching all Users")
		err_msg := ErrMessage{
			Status:  "ERROR",
			Message: "Error when fetching all users",
			Code:    http.StatusInternalServerError,
		}
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err_msg)
	}

	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func GetUserById(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userId := vars["userId"]
	id, err := strconv.ParseInt(userId, 0, 0)
	if err != nil {
		fmt.Println("Error while parsing")
	}

	userDetails, _ := models.GetUserById(id)
	res, err := json.Marshal(userDetails)
	if err != nil {
		fmt.Println("Error when fetching user")
		err_msg := ErrMessage{
			Status:  "ERROR",
			Message: "Error when fetching user",
			Code:    http.StatusInternalServerError,
		}
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err_msg)
	}

	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	var updateUser = &models.User{}
	utils.ParseBody(r, updateUser)
	vars := mux.Vars(r)
	userId := vars["userId"]
	id, err := strconv.ParseInt(userId, 0, 0)
	key := "tes123"

	if err != nil {
		fmt.Println("Error when updating user")
		err_msg := ErrMessage{
			Status:  "ERROR",
			Message: "Error when updating user",
			Code:    http.StatusInternalServerError,
		}
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err_msg)
	}

	userDetails, db := models.GetUserById(id)
	if updateUser.Username != "" {
		userDetails.Username = updateUser.Username
	}
	if updateUser.Password != "" {
		userDetails.Password = updateUser.Password
	}

	_ = db.Exec("UPDATE users SET username = ?, password = AES_ENCRYPT(?, ?) WHERE id = ?",
		userDetails.Username, userDetails.Password, key, id)
	res, err := json.Marshal(userDetails)
	if err != nil {
		fmt.Println("Error while parsing!!!")
	}

	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userId := vars["userId"]
	id, err := strconv.ParseInt(userId, 0, 0)

	if err != nil {
		fmt.Println("Error when deleting user")
		err_msg := ErrMessage{
			Status:  "ERROR",
			Message: "Error when deleting user",
			Code:    http.StatusInternalServerError,
		}
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err_msg)
	}

	user := models.DeleteUser(id)
	res, err := json.Marshal(user)
	if err != nil {
		fmt.Println("Error while parsing!!!")
	}

	w.Header().Set("Content-Type", "pkglication/json")
	w.WriteHeader(http.StatusOK)
	w.Write(res)
}

func Login(w http.ResponseWriter, r *http.Request) {
	Login := &models.User{}
	utils.ParseBody(r, Login)
	u := Login.Login()
	res, err := json.Marshal(u)
	if err != nil {
		err_msg := ErrMessage{
			Status:  "ERROR",
			Message: "Wrong Username or Password!",
			Code:    http.StatusUnauthorized,
		}
		w.WriteHeader(http.StatusUnauthorized)
		json.NewEncoder(w).Encode(err_msg)
	}

	w.WriteHeader(http.StatusOK)
	w.Write(res)
}
