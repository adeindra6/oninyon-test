package models

import (
	"time"

	"net/http"

	"github.com/adeindra6/oninyon-test/pkg/config"
	jwt "github.com/dgrijalva/jwt-go"
	"gorm.io/gorm"
)

var db *gorm.DB

type User struct {
	gorm.Model
	Username string `gorm:""json:"username"`
	Password string `gorm:"type:varbinary(200)"json:"password"`
}

type LoginRes struct {
	Token  string    `gorm:""json:"token"`
	Status string    `json:"status"`
	Code   int64     `json:"code"`
	Exp    time.Time `json:"exp"`
}

func init() {
	config.Connect()
	db = config.GetDB()
	db.AutoMigrate(&User{})
}

func (u *User) CreateUser() *User {
	key := "tes123"
	_ = db.Exec("INSERT INTO users(username, password) VALUES (?, AES_ENCRYPT(?, ?))", u.Username, u.Password, key)

	return u
}

func GetAllUser() []User {
	var Users []User
	db.Find(&Users)
	return Users
}

func GetUserById(id int64) (*User, *gorm.DB) {
	var getUser User
	db := db.Where("id = ?", id).Find(&getUser)
	return &getUser, db
}

func DeleteUser(id int64) User {
	var user User
	db.Where("id = ?", id).Delete(&user)
	return user
}

func (u *User) Login() *LoginRes {
	var LoginUser User
	var Token LoginRes
	key := "tes123"
	db.Raw("SELECT username, AES_DECRYPT(password, ?) FROM users WHERE username = ?", key, u.Username).Scan(&LoginUser)

	if LoginUser.Password == u.Password {
		expTime := time.Now().Add(time.Hour * 24)
		jwtToken, err := CreateJWTToken(LoginUser.ID, LoginUser.Username, expTime)
		if err != nil {
			panic(err)
		}

		Token.Token = jwtToken
		Token.Status = "SUCCESS"
		Token.Code = http.StatusOK
		Token.Exp = expTime
	} else {
		Token.Token = ""
		Token.Status = "FAILURE"
		Token.Code = http.StatusBadRequest
		Token.Exp = time.Now()
	}

	return &Token
}

func CreateJWTToken(id uint, username string, expTime time.Time) (string, error) {
	var err error

	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["user_id"] = id
	atClaims["username"] = username
	atClaims["exp"] = expTime.Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS512, atClaims)

	token, err := at.SignedString([]byte("tes123"))
	if err != nil {
		return "", err
	}

	return token, nil
}
